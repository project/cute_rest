<?php

namespace Drupal\cute_user;

use Drupal;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Session\AccountProxyInterface;

/**
 * Class UserService.
 */
class UserService implements UserServiceInterface {

  /**
   * Drupal\Core\Session\AccountProxyInterface definition.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new UserService object.
   */
  public function __construct(
    AccountProxyInterface      $current_user,
    EntityTypeManagerInterface $entity_type_manager
  ) {
    $this->currentUser = $current_user;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * @param $data
   *
   * @return array
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function createUser($data) {
    $user = $this->entityTypeManager->getStorage('user')->create($data);
    $access = $this->checkAccess($user, 'create');
    if ($access) {
      $user->save();
      return ["status" => 200, "data" => $user->toArray()];
    }
    return ["status" => 403, "data" => "Access denied"];
  }

  /**
   * @param $user
   * @param $operation
   *
   * @return bool
   */
  private function checkAccess($user, $operation) {
    return $user->access($operation, $this->currentUser);
  }

  /**
   * @param $uid
   *
   * @return array
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function deleteUser($uid) {
    $user = Drupal::entityTypeManager()->getStorage('user')->load($uid);
    if ($user) {
      $access = $this->checkAccess($user, 'delete');
      if ($access) {
        $user->delete();
        return ["status" => 200, "data" => "User deleted"];
      }
      return ["status" => 403, "data" => "Access denied"];
    }
    return ["status" => 404, "data" => "User not found"];

  }

  /**
   * @param $uid
   *
   * @return array
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getUser($uid) {
    // get user by uid
    $user = Drupal::entityTypeManager()->getStorage('user')->load($uid);
    if ($user) {
      $access = $this->checkAccess($user, 'view');
      if ($access) {
        return ["status" => 200, "data" => $user->toArray()];
      }
      return ["status" => 403, "data" => "Access denied"];
    }
    return ["status" => 404, "data" => "User not found"];
  }

  public function getUserName($uid) {
    $user = Drupal::entityTypeManager()->getStorage('user')->load($uid);
    if ($user) {
      $access = $this->checkAccess($user, 'view');
      $userNae = $user->realname ? $user->realname : $user->name->value;
      if ($access) {
        return ["status" => 200, "data" => $userNae];
      }
      return ["status" => 403, "data" => "Access denied"];
    }
  }

  /**
   * @param $data
   *
   * @return array
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function updateUser($data) {
    // update user by uid
    $user = Drupal::entityTypeManager()->getStorage('user')->load($data['uid']);
    if ($user) {
      $access = $this->checkAccess($user, 'update');
      if ($access) {
        foreach ($data as $key => $value) {
          $user->set($key, $value);
        }
        $user->save();
        return ["status" => 200, "data" => $user->toArray()];
      }
      return ["status" => 403, "data" => "Access denied"];
    }
    return ["status" => 404, "data" => "User not found"];
  }


  /**
   * @return array
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getAllUser() {
    // get all users
    $users = Drupal::entityTypeManager()->getStorage('user')->loadMultiple();


    $result = [];
    foreach ($users as $user) {
      if ($user->id() !=0){
        $result[] = [
          'id' => $user->id(),
          'name' => $user->realname ? $user->realname : $user->get('name')
            ->getString(),
        ];
      }

    }
    return ["status" => 200, "data" => $result];
  }

  public function login($username, $password) {
  }

  public function userRegister($data) {
  }

  public function oauthBySms($data) {
    // user oauth by sms
  }

}
