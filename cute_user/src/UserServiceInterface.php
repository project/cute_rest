<?php

namespace Drupal\cute_user;

/**
 * Interface UserServiceInterface.
 */
interface UserServiceInterface {

  public function getUser($uid);

  public function getUserName($uid);

  public function getAllUser();

  public function createUser($data);

  public function deleteUser($uid);

  public function updateUser($data);

  public function login($username, $password);

  public function userRegister($data);

  public function oauthBySms($data);

}
